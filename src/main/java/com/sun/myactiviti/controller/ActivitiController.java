package com.sun.myactiviti.controller;

import com.sun.myactiviti.config.Base64Convert;
import com.sun.myactiviti.entity.ProcessDefinitionVo;
import com.sun.myactiviti.entity.R;
import com.sun.myactiviti.service.ActivitiService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.repository.Deployment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.InputStream;
import java.util.List;

/**
 * @author:TT
 * @date 2021/09-08 15:59
 */
@Slf4j
@RestController
@RequestMapping("/activiti")
public class ActivitiController {

    @Autowired
    private ActivitiService activitiService;

    /**
     * Description: 手动部署工作流
     */
    @GetMapping(value = "/deploymentProcessDefinition")
    public R deploymentProcessDefinition(String deploymentName, String categoryId, String bpmnFileName) {
        R error = R.error("部署失败");
        try {
            Deployment deployment = activitiService.deploymentProcessDefinition(deploymentName, categoryId, bpmnFileName);
            return R.ok().put("data", deployment);
        } catch (Exception e) {
            e.printStackTrace();
            error.put("data", e.getMessage());
        }
        return error;
    }

    /**
     * Description: 查询已经部署的工作流
     */
    @GetMapping(value = "/getAllDeploymentProcessDefinition")
    public R getAllDeploymentProcessDefinition() {
        List<ProcessDefinitionVo> allDeploymentProcessDefinition = activitiService.getAllDeploymentProcessDefinition();
        return R.ok().put("data", allDeploymentProcessDefinition);
    }

    /**
     * 删除流程定义
     *
     * @param processDefinitionKey
     * @param cascade
     * @return
     */
    @GetMapping(value = "/deleteProcessDefinitionByKey/{processDefinitionKey}/{cascade}")
    public R deleteProcessDefinitionByKey(@PathVariable("processDefinitionKey") String processDefinitionKey, @PathVariable("cascade") Boolean cascade) {
        try {
            activitiService.deleteProcessDefinitionByKey(processDefinitionKey, cascade);
            return R.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return R.error(e.getMessage());
        }
    }

    /**
     * 根据分类查询流程图
     *
     * @param categoryId 分类id
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/viewPic")
    public R viewPic(@RequestParam String categoryId) throws Exception {
        InputStream inputStream = activitiService.viewPic(categoryId);
        if (inputStream != null) {
            String base64 = Base64Convert.ioToBase64(inputStream);
            log.debug("查找成功");
            return R.ok().put("data", base64);
        } else {
            log.error("没查询到流程图");
            return R.error("不存在流程图");
        }
    }

    /**
     * Description: 启动工作流
     */
    @GetMapping("/startProcess")
    public R startProcess(String businessKey, String processDefinitionKey, String variables) {
        try {
            return activitiService.startProcess(businessKey, processDefinitionKey, variables);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return R.error("启动任务失败");
    }

    /**
     * 查询个人待办任务
     *
     * @param userId
     * @return
     */
    @GetMapping("/getTasksByUserId")
    public R getTasksByUserId(@RequestParam String userId, String businessKey) {
        try {
            return R.ok().put("data", activitiService.getTasksByUserId(businessKey, userId));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return R.error("查询任务失败");
    }

    /**
     * Description: 完成任务
     */
    @GetMapping("/completeTask")
    public R completeTask(String businessKey, String variables) {
        try {
            return activitiService.completeTask(businessKey, variables);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return R.error("提交任务失败");
    }

    /**
     * Description: 获取流程连线名称
     */
    @GetMapping("/getOutComeListByBusinessKey/{businessKey}")
    public R getOutComeListByBusinessKey(@PathVariable("businessKey") String businessKey) {
        try {
            return activitiService.getOutComeListByBusinessKey(businessKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return R.error("获取失败");
    }

    /**
     * Description: 获取审核意见
     */
    @GetMapping("/getCommonList/{businessKey}/{userId}")
    public R getCommonList(@PathVariable("businessKey") String businessKey, @PathVariable("userId") String userId) {
        try {
            return activitiService.getCommonList(businessKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return R.error("获取失败");
    }

    /**
     * Description: 获取历史任务
     */
    @GetMapping("/getHistoryTask/{userId}")
    public R getHistoryTask(@PathVariable("userId") String userId) {
        try {
            return activitiService.getHistoryTask(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return R.error("获取历史任务失败");
    }

    /**
     * Description: 获取历史流程实例
     */
    @GetMapping("/getHistoryProcessInstance")
    public R getHistoryProcessInstance() {
        try {
            return activitiService.getHistoryProcessInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return R.error("获取历史流程实例失败");
    }

    /**
     * Description: 删除流程任务
     */
    @GetMapping("/deleteProcessInstance/{businessKey}/{reason}")
    public R deleteProcessInstance(@PathVariable("businessKey") String businessKey, @PathVariable("reason") String reason) {
        try {
            return activitiService.deleteProcessInstance(businessKey, reason);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return R.error("撤销流程任务失败");
    }

    /**
     * Description: 获取历史流程实例图(不包含进度)
     */
    @GetMapping("/viewCurrentPic/{businessKey}")
    public R viewCurrentPic(@PathVariable("businessKey") String businessKey) {
        InputStream inputStream = activitiService.viewCurrentPic(businessKey);
        if (inputStream != null) {
            String base64 = null;
            try {
                base64 = Base64Convert.ioToBase64(inputStream);
                log.debug("查找成功");
                return R.ok().put("data", base64);
            } catch (Exception e) {
                e.printStackTrace();
                return R.error("不存在流程图");
            }
        } else {
            log.error("没查询到流程图");
            return R.error("不存在流程图");
        }
    }

    /**
     * Description: 获取历史流程实例图(包含进度)
     */
    @GetMapping("/viewCurrentPics/{businessKey}")
    public R viewCurrentPics(@PathVariable("businessKey") String businessKey) {
        InputStream inputStream = activitiService.viewCurrentPics(businessKey);
        if (inputStream != null) {
            String base64 = null;
            try {
                base64 = Base64Convert.ioToBase64(inputStream);
                log.debug("查找成功");
                return R.ok().put("data", base64);
            } catch (Exception e) {
                e.printStackTrace();
                return R.error("不存在流程图");
            }
        } else {
            log.error("没查询到流程图");
            return R.error("不存在流程图");
        }
    }

}