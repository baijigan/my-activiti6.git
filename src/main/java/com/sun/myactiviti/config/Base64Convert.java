package com.sun.myactiviti.config;


import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

/**
 * InputStreamBase64 工具类<br>
 **/

public class Base64Convert {

    public static String ioToBase64(InputStream inputStream) throws IOException {
        byte[] bytes = IOUtils.toByteArray(inputStream);
        String base64 = Base64.getEncoder().encodeToString(bytes);
        inputStream.close();
        return base64;
    }

    public static InputStream base64ToIo(String strBase64) throws IOException {
        // 解码，然后将字节转换为流
        byte[] bytes = Base64.getDecoder().decode(strBase64);
        return new ByteArrayInputStream(bytes);
    }
}
