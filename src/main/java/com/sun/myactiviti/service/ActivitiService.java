package com.sun.myactiviti.service;

import com.sun.myactiviti.entity.ProcessDefinitionVo;
import com.sun.myactiviti.entity.R;
import com.sun.myactiviti.entity.TaskResVo;
import org.activiti.engine.repository.Deployment;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * <p></p>
 *
 * @author TT
 * @date 2021-07-21 21:13
 */
public interface ActivitiService {

    /**
     * 功能描述: 部署流程<br>
     *
     * @param deploymentName 部署名称
     * @param bpmnFileName   部署资源，当对于，resource/processes/文件夹
     * @return Deployment 部署对象
     */
    Deployment deploymentProcessDefinition(String deploymentName, String categoryId, String bpmnFileName);

    /**
     * 功能描述: 查询已经部署的工作流
     */
    List<ProcessDefinitionVo> getAllDeploymentProcessDefinition();

    /**
     * 功能描述: 删除流程定义<br>
     *
     * @param processDefinitionKey 流程定义key
     * @param cascade              是否级联
     * @return:void
     */
    void deleteProcessDefinitionByKey(String processDefinitionKey, Boolean cascade);

    /**
     * 功能描述: 根据分类查询流程图<br>
     *
     * @param categoryId
     * @return:java.io.InputStream
     */
    InputStream viewPic(String categoryId);

    /**
     * 功能描述: 启动流程实例<br>
     *
     * @param businessKey          业务主键
     * @param processDefinitionKey 流程定义KEY (bpmn文件的id)
     * @param variables            流程变量 (HashMap的Json串)
     * @return:org.activiti.engine.runtime.ProcessInstance
     */
    R startProcess(String businessKey, String processDefinitionKey, String variables);

    /**
     * 功能描述: 完成任务<br>
     *
     * @param businessKey
     * @param variables   HashMap(Json格式) 必传参数:
     *                    currentUser:当前节点负责人
     *                    userId:下一节点负责人
     *                    userIds:下一节点负责人多个(用 ',' 分割)
     *                    common:备注
     * @return:void
     */
    R completeTask(String businessKey, String variables);

    /**
     * 功能描述: 完成任务<br>
     *
     * @param businessKey
     * @param variables   HashMap 必传参数:
     *                    currentUser:当前节点负责人
     *                    userId:下一节点负责人
     *                    userIds:下一节点负责人多个(用 ',' 分割)
     *                    common:备注
     * @return:void
     */
    R completeTask(String businessKey, Map<String, Object> variables);

    /**
     * 功能描述: 查询个人代办任务<br>
     *
     * @param businessKey
     * @param userId
     * @return:java.util.List<org.activiti.engine.task.Task>
     */
    List<TaskResVo> getTasksByUserId(String businessKey, String userId);

    /**
     * 功能描述: 根据业务主键查询流程连线集合<br>
     *
     * @param businessKey
     * @return:R
     */
    R getOutComeListByBusinessKey(String businessKey);

    /**
     * 功能描述: 根据业务ID获取审核批注<br>
     *
     * @param businessKey
     * @return:R
     */
    R getCommonList(String businessKey);

    /**
     * 功能描述: 根据用户ID查询此用户历史任务<br>
     *
     * @param userId
     * @return:R
     */
    R getHistoryTask(String userId);

    /**
     * 功能描述: 查询历史流程实例<br>
     *
     * @param
     * @return:R
     */
    R getHistoryProcessInstance();

    /**
     * 功能描述: 删除流程任务 记录在ACT_HI_PROCINST<br>
     *
     * @param businessKey
     * @param reason
     * @return:R
     */
    R deleteProcessInstance(String businessKey, String reason);

    /**
     * 功能描述: 根据业务ID查询流程图(不包含进度)
     *
     * @param businessKey
     * @return:java.io.InputStream
     */
    InputStream viewCurrentPic(String businessKey);

    /**
     * 功能描述: 根据业务ID查询流程图(包含进度)
     *
     * @param businessKey
     * @return
     */
    InputStream viewCurrentPics(String businessKey);


}


