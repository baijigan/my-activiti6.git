package com.sun.myactiviti.n_task.servicetask;

import com.sun.myactiviti.ApplicationTests;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>脚本任务</p>
 *
 * @author TT
 * @date 2021-09-19 16:32
 */
public class JavaServiceTaskTest extends ApplicationTests {
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private RuntimeService runtimeService;

    private final String bpmnNameAndKey = "javaServiceTask";

    @Test
    public void deployment() {
        Deployment deployment = repositoryService.createDeployment().name("请假流程")
                .addClasspathResource("processes/" + bpmnNameAndKey + ".bpmn")
                .addClasspathResource("processes/" + bpmnNameAndKey + ".png")
                .category("HR")
                .deploy();
        System.out.println("部署ID\t" + deployment.getId());
        System.out.println("部署分类\t" + deployment.getCategory());
        System.out.println("部署名称\t" + deployment.getName());
    }

    /*
    * <serviceTask id="servicetask1" name="java脚本任务" activiti:class="com.sun.myactiviti.n_task.servicetask.MyJavaDelegate"></serviceTask>
    * 启动后进入的serviceTask，脚本任务自动执行，可以在脚本中自定义逻辑
     */

    @Test
    public void start() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("input", "VXBB");
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(bpmnNameAndKey, variables);
        System.out.println("流程实例ID\t" + processInstance.getId());
        System.out.println("流程定义ID\t" + processInstance.getProcessDefinitionId());
        System.out.println("流程定义KEY\t" + processInstance.getProcessDefinitionKey());
    }

}
