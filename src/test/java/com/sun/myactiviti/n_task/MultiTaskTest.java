package com.sun.myactiviti.n_task;

import com.sun.myactiviti.ApplicationTests;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p></p>
 *
 * @author TT
 * @date 2021-08-28 23:06
 */
public class MultiTaskTest extends ApplicationTests {
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private RuntimeService runtimeService;
    @Resource
    private TaskService taskService;

    private final String bpmnNameAndKey = "multiTask";

    @Test
    public void deployment() {
        Deployment deployment = repositoryService.createDeployment().name("请假流程")
                .addClasspathResource("processes/" + bpmnNameAndKey + ".bpmn")
                .addClasspathResource("processes/" + bpmnNameAndKey + ".png")
                .category("HR")
                .deploy();
        System.out.println("部署ID\t" + deployment.getId());
        System.out.println("部署分类\t" + deployment.getCategory());
        System.out.println("部署名称\t" + deployment.getName());
    }

    @Test
    public void start() {
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(bpmnNameAndKey);
        System.out.println("流程实例ID\t" + processInstance.getId());
        System.out.println("流程定义ID\t" + processInstance.getProcessDefinitionId());
        System.out.println("流程定义KEY\t" + processInstance.getProcessDefinitionKey());
    }

    @Test
    public void findMyTask() {
        String assignee = "王五";
        List<Task> taskList = taskService.createTaskQuery()
                .taskAssignee(assignee)
                .list();

        for (Task task : taskList) {
            System.out.println("任务ID\t" + task.getId());
            System.out.println("任务名称\t" + task.getName());
            System.out.println("流程定义ID\t" + task.getProcessDefinitionId());
            System.out.println("流程实例ID\t" + task.getProcessInstanceId());
            System.out.println("执行对象ID\t" + task.getExecutionId());
            System.out.println("任务创办时间\t" + task.getCreateTime());
            System.out.println("=======");
        }
    }

    /*
    * 会签流程实例
    * <userTask id="usertask2" name="会签【委办局领导】" activiti:assignee="${inputUser}">
    *    <multiInstanceLoopCharacteristics isSequential="false" activiti:collection="inputUserList" activiti:elementVariable="inputUser"></multiInstanceLoopCharacteristics>
    * </userTask>
    * 提交到会签节点，传递会签人员名单
    *
     */

    @Test
    public void compleTask() {
        String taskId = "92505";
        Map<String, Object> variables = new HashMap<>();
        variables.put("inputUserList", Arrays.asList("李四", "王五"));
        taskService.complete(taskId, variables);
    }

}
