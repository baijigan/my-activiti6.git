package com.sun.myactiviti.k_grouptask;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

/**
 * <p></p>
 *
 * @author TT
 * @date 2021-07-29 23:45
 */
public class TaskListenerImpl implements TaskListener {

    /*
    通过如下定义侦听事件
    <userTask id="usertask1" name="审批">
      <extensionElements>
        <activiti:taskListener event="create" class="com.mashibing.activiti.grouptask.TaskListenerImpl"></activiti:taskListener>
      </extensionElements>
    </userTask>
    */

    @Override
    public void notify(DelegateTask delegateTask) {
        /*指定个人任务的办理人，组任务办理人*/

        /*  */
        /*可以查询数据库*/
        delegateTask.addCandidateUser("小a");
        delegateTask.addCandidateUser("小b");
    }
}
