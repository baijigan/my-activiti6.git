package com.sun.myactiviti.o_bpmn.e_compensation;

import com.sun.myactiviti.ApplicationTests;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

/**
 * <p></p>
 *
 * @author TT
 * @date 2021-09-14 20:56
 */
public class CompensationTest extends ApplicationTests {
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private RuntimeService runtimeService;

    private final String bpmnNameAndKey = "compensation";

    @Test
    public void deployment() {
        Deployment deployment = repositoryService.createDeployment().name("请假流程")
                .addClasspathResource("processes/" + bpmnNameAndKey + ".bpmn")
                .addClasspathResource("processes/" + bpmnNameAndKey + ".png")
                .category("HR")
                .deploy();
        System.out.println("部署ID\t" + deployment.getId());
        System.out.println("部署分类\t" + deployment.getCategory());
        System.out.println("部署名称\t" + deployment.getName());
    }

    /*
    * 补偿信号事件
    * 通过中间信号事件发出信号，再通过边界事件接收到信号，然后执行serviceTask
    *
    *     <serviceTask id="servicetask2" name="农业银行加款" activiti:class="com.sun.myactiviti.o_bpmn.e_compensation.AbcAddDelegate"></serviceTask>
    <intermediateThrowEvent id="compensationintermediatethrowevent1" name="CompensationThrowingEvent">
      <compensateEventDefinition></compensateEventDefinition>
    </intermediateThrowEvent>
    *
    * 上面发出信号，下面收到补偿边界事件
    *
    *     <serviceTask id="servicetask4" name="农业银行处理补偿" activiti:class="com.sun.myactiviti.o_bpmn.e_compensation.AbcReduceDelegate" isForCompensation="true"></serviceTask>
    <boundaryEvent id="boundarycompensation1" name="Compensate" attachedToRef="servicetask1" cancelActivity="true">
      <compensateEventDefinition></compensateEventDefinition>
    </boundaryEvent>
     */

    @Test
    public void start() {
        //启动流程
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(bpmnNameAndKey);
        System.out.println("流程实例ID\t" + processInstance.getId());
        System.out.println("流程实例ID\t" + processInstance.getProcessInstanceId());
        System.out.println("流程定义ID\t" + processInstance.getProcessDefinitionId());
    }
}
