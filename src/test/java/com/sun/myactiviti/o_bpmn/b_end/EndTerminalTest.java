package com.sun.myactiviti.o_bpmn.b_end;

import com.sun.myactiviti.ApplicationTests;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

/**
 * <p>中止结束事件</p>
 *
 * @author TT
 * @date 2021-07-27 22:33
 */
public class EndTerminalTest extends ApplicationTests {
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private RuntimeService runtimeService;
    @Resource
    private TaskService taskService;

    private final String bpmnNameAndKey = "endTerminal";
    @Test
    public void deployment() {
        Deployment deployment = repositoryService.createDeployment().name("请假流程")
                .addClasspathResource("processes/" + bpmnNameAndKey + ".bpmn")
                .addClasspathResource("processes/" + bpmnNameAndKey + ".png")
                .category("HR")
                .deploy();
        System.out.println("部署ID\t"+deployment.getId());
        System.out.println("部署分类\t"+deployment.getCategory());
        System.out.println("部署名称\t"+deployment.getName());
    }
    @Test
    public void start() {
        //启动流程
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(bpmnNameAndKey);
        System.out.println("流程实例ID\t"+processInstance.getId());
        System.out.println("流程实例ID\t"+processInstance.getProcessInstanceId());
        System.out.println("流程定义ID\t"+processInstance.getProcessDefinitionId());
    }

    /*
    * EndEvent分为4种类型，分别表示在不同的结束事件下的动作。
    * 1、EndEvent
    * 2、ErrorEndEvent
    * 3、TerminateEndEvent
    * 4、CancelEndEvent
    *
    * TerminateEndEvent(终止结束事件)
    * 终止结束事件表示为结束事件，具有terminateEventDefinition子元素。terminateAll属性是可选的，默认情况下为false。
    * 可以添加可选属性terminateAll。如果为true，则无论在流程定义中是否放置终止结束事件，并且无论是否处于子流程（甚至是嵌套）、（根）流程实例都将终止。
    * <endEvent id="terminateendevent1" name="TerminateEndEvent">
    *    <terminateEventDefinition></terminateEventDefinition>
    * </endEvent>
    * */

    @Test
    public void compleTask() {
        String taskId = "40007";
        taskService.complete(taskId);

    }
}
