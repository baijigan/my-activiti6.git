package com.sun.myactiviti.o_bpmn.b_end;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

/**
 * <p></p>
 *
 * @author TT
 * @date 2021-09-08 22:20
 */
public class CancelDelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("取消事件");
    }
}
