package com.sun.myactiviti.o_bpmn.c_boundary;

import com.sun.myactiviti.ApplicationTests;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

/**
 * <p>信号边界事件</p>
 *
 * @author TT
 * @date 2021-07-27 22:33
 */
public class BoundarySignalTest extends ApplicationTests {
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private RuntimeService runtimeService;
    @Resource
    private TaskService taskService;

    private final String bpmnNameAndKey = "boundarySignal";
    @Test
    public void deployment() {
        Deployment deployment = repositoryService.createDeployment().name("请假流程")
                .addClasspathResource("processes/" + bpmnNameAndKey + ".bpmn")
                .addClasspathResource("processes/" + bpmnNameAndKey + ".png")
                .category("HR")
                .deploy();
        System.out.println("部署ID\t"+deployment.getId());
        System.out.println("部署分类\t"+deployment.getCategory());
        System.out.println("部署名称\t"+deployment.getName());
    }
    @Test
    public void start() {
        //启动流程
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(bpmnNameAndKey);
        System.out.println("流程实例ID\t"+processInstance.getId());
        System.out.println("流程实例ID\t"+processInstance.getProcessInstanceId());
        System.out.println("流程定义ID\t"+processInstance.getProcessDefinitionId());
    }

    @Test
    public void compleTask() {
        String taskId = "10005";
        taskService.complete(taskId);
    }

    /*
    * 边界信号事件
    * 假设当前有一个签订合同的流程，会先进行合同的查看，然后再进行合同确认，如果在合同确认时接收到信息，
    * 合同的条款发生变更，那么就会对业务流程产生影响（不能再进行签订合同或者重新查看合同条款）
    *
    * 也就是在某个节点收到了一个信号，则自动切换到另一个节点。
    * 本示例执行失败
    * <boundaryEvent id="boundarysignal1" name="Signal" attachedToRef="usertask2" cancelActivity="true">
    *    <signalEventDefinition signalRef="contactChangeSignal"></signalEventDefinition>
    * </boundaryEvent>
     */

    @Test
    public void signal(){
        runtimeService.signalEventReceived("contactChangeSignal");
    }
}
