package com.sun.myactiviti.o_bpmn.d_intermediate;

import com.sun.myactiviti.ApplicationTests;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

/**
 * <p>信号中间事件</p>
 *
 * @author TT
 * @date 2021-07-27 22:33
 */
public class IntermediateSignalTest extends ApplicationTests {
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private RuntimeService runtimeService;
    @Resource
    private TaskService taskService;

    private final String bpmnNameAndKey = "intermediateSignal";
    @Test
    public void deployment() {
        Deployment deployment = repositoryService.createDeployment().name("请假流程")
                .addClasspathResource("processes/" + bpmnNameAndKey + ".bpmn")
                .addClasspathResource("processes/" + bpmnNameAndKey + ".png")
                .category("HR")
                .deploy();
        System.out.println("部署ID\t"+deployment.getId());
        System.out.println("部署分类\t"+deployment.getCategory());
        System.out.println("部署名称\t"+deployment.getName());
    }
    @Test
    public void start() {
        //启动流程
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(bpmnNameAndKey);
        System.out.println("流程实例ID\t"+processInstance.getId());
        System.out.println("流程实例ID\t"+processInstance.getProcessInstanceId());
        System.out.println("流程定义ID\t"+processInstance.getProcessDefinitionId());
    }

    /*
    * 中间信号事件
    * 当审批到达一个中间信号节点后，会抛出finashPay事件，用于订单支付成功发出通知
    * <intermediateThrowEvent id="signalintermediatethrowevent1" name="SignalThrowEvent">
    *     <signalEventDefinition signalRef="finashPay"></signalEventDefinition>
    *  </intermediateThrowEvent>
    *
    *  仓库和积分节点前面会有一个等待信号的节点
    *  <intermediateCatchEvent id="signalintermediatecatchevent1" name="SignalCatchEvent">
    *     <signalEventDefinition signalRef="finashPay"></signalEventDefinition>
    *  </intermediateCatchEvent>
    *  获取到finashPay事件后，进行到下个节点同时到达仓库和积分。
     */

    @Test
    public void compleTask() {
        String taskId = "30009";
        taskService.complete(taskId);
    }

}
