package com.sun.myactiviti.p_subprocess;

import com.sun.myactiviti.ApplicationTests;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

/**
 * <p></p>
 *
 * @author TT
 * @date 2021-09-14 20:56
 */
public class CallSubProcessTest extends ApplicationTests {
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private RuntimeService runtimeService;
    @Resource
    private TaskService taskService;
    private final String bpmnNameAndKey1 = "callSubProcess";
    private final String bpmnNameAndKey2 = "mainProcess";

    @Test
    public void deployment() {
        Deployment deployment = repositoryService.createDeployment().name("请假流程")
                .addClasspathResource("processes/" + bpmnNameAndKey1 + ".bpmn")
                .addClasspathResource("processes/" + bpmnNameAndKey1 + ".png")

                .addClasspathResource("processes/" + bpmnNameAndKey2 + ".bpmn")
                .addClasspathResource("processes/" + bpmnNameAndKey2 + ".png")
                .category("HR")
                .deploy();
        System.out.println("部署ID\t" + deployment.getId());
        System.out.println("部署分类\t" + deployment.getCategory());
        System.out.println("部署名称\t" + deployment.getName());
    }

    @Test
    public void start() {
        //启动流程
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(bpmnNameAndKey2);
        System.out.println("流程实例ID\t" + processInstance.getId());
        System.out.println("流程实例ID\t" + processInstance.getProcessInstanceId());
        System.out.println("流程定义ID\t" + processInstance.getProcessDefinitionId());
    }

    /*
     * 从主流程启动后，进入调用子流程，主子分别是两个流程定义文件
     * 执行mainProcess，然后进入到 callSubProcess
     */

    @Test
    public void compleTask() {
        String taskId = "2505";
        taskService.complete(taskId);

    }
}
