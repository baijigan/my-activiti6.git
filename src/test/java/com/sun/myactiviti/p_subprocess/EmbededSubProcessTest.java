package com.sun.myactiviti.p_subprocess;

import com.sun.myactiviti.ApplicationTests;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

/**
 * <p></p>
 *
 * @author TT
 * @date 2021-09-14 20:56
 */
public class EmbededSubProcessTest extends ApplicationTests {
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private RuntimeService runtimeService;

    private final String bpmnNameAndKey = "embededSubProcess";

    @Test
    public void deployment() {
        Deployment deployment = repositoryService.createDeployment().name("请假流程")
                .addClasspathResource("processes/" + bpmnNameAndKey + ".bpmn")
                .addClasspathResource("processes/" + bpmnNameAndKey + ".png")
                .category("HR")
                .deploy();
        System.out.println("部署ID\t" + deployment.getId());
        System.out.println("部署分类\t" + deployment.getCategory());
        System.out.println("部署名称\t" + deployment.getName());
    }

    /*
    * 这是内嵌子流程，主子流程定义在一个文件里
    * <subProcess id="subprocess1" name="Sub Process">
          <startEvent id="startevent2" name="Start"></startEvent>
          <endEvent id="endevent1" name="End"></endEvent>
          <serviceTask id="servicetask1" name="Service Task" activiti:class="com.sun.myactiviti.p_subprocess.ErrorDelegate"></serviceTask>
          <sequenceFlow id="flow4" sourceRef="startevent2" targetRef="servicetask1"></sequenceFlow>
          <sequenceFlow id="flow5" sourceRef="servicetask1" targetRef="endevent1"></sequenceFlow>
       </subProcess>
       <boundaryEvent id="boundaryerror1" name="Error" attachedToRef="subprocess1">
          <errorEventDefinition></errorEventDefinition>
       </boundaryEvent>
    *
    * 子流程执行完成后通过边界事件发送信号完成
     */


    @Test
    public void start() {
        //启动流程
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(bpmnNameAndKey);
        System.out.println("流程实例ID\t" + processInstance.getId());
        System.out.println("流程实例ID\t" + processInstance.getProcessInstanceId());
        System.out.println("流程定义ID\t" + processInstance.getProcessDefinitionId());
    }
}
