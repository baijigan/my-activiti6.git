package com.sun.myactiviti.b_first;

import com.sun.myactiviti.ApplicationTests;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.jupiter.api.Test;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p></p>
 *
 * @author TT
 * @date 2021-06-23 22:33
 */
public class ActivitiQuick extends ApplicationTests {

    @Resource
    private RepositoryService repositoryService;
    @Resource
    private RuntimeService runtimeService;
    @Resource
    private TaskService taskService;
    @Resource
    private HistoryService historyService;

    private final String bpmnNameAndKey = "first";

    /*见表，默认启动的时候就会建好表格*/
    @Test
    public void createTable() {

    }

    /*
    * 1、表单初始设置
    * 建立表单和流程之间的关系，给表单选择一个已经部署的流程
    * 表单Id  ：
    * 表单Name    :
    * 流程部署Id    :
    * 流程KeyId     :
    * 流程名称    :
    * 流程分类Id    :
    *
    * 命令行执行： mvn test -D=ActivitiQuick#deploy
    * 这样可以逐一运行
    * */

    /*第一步：--永远是画制流程图---部署流程图*/
    @Test
    public void deploy() {
        Deployment deploy = repositoryService.createDeployment()
                .addClasspathResource("processes/first.bpmn")
                .addClasspathResource("processes/first.png")
                .key(bpmnNameAndKey)
                .name(bpmnNameAndKey + "name")
                .category("HR")
                .deploy();
        System.out.println("流程部署ID\t" + deploy.getId());
        System.out.println("流程keyID\t" + deploy.getKey());
        System.out.println("流程名称\t" + deploy.getName());
        System.out.println("流程分类ID\t" + deploy.getCategory());
    }

    /*
    * 2、表单启动流程
    * 表单启动流程根据关联关系找到流程KeyId，进行发起流程
    * 发起流程后获取到流程实例Id记录到当前记录的主子表单的主表上
    * 主表UniqueId    :
    * 流程keyId      :
    * 流程实例Id    :
    * 流程状态  ：  执行中/结束（每次操作后查询）
    *
     */

    /*第二步：启动一个流程实例，相当于----有人请假一次*/
    @Test
    public void start() {
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(bpmnNameAndKey);
        System.out.println("流程实例ID\t" + processInstance.getId());
        System.out.println("流程定义ID\t" + processInstance.getProcessDefinitionId());
        System.out.println("流程定义key\t" + processInstance.getProcessDefinitionKey());
    }

    /*
    * 3、查询代办调整到表单显示
    * 根据流程定义key找到对应的formId，再根据 流程定义KeyId + 流程实例Id，到主表中查找到对应主表记录。
    * 如果存在多个表单用同一个流程的现象，
    * 循环找到流程定义key的下一个formId，.....
    * 最后确定了formId和主表的记录UniqueId进行显示
     */

    /*第三步:查找个人待办任务列表*/
    @Test
    public void findMyTask() {
        String assignee = "张三";
        List<Task> list = taskService.createTaskQuery()
                .taskAssignee(assignee)
                .list();
        if (!CollectionUtils.isEmpty(list)) {
            for (Task task : list) {
                System.out.println("任务ID\t" + task.getId());
                System.out.println("任务名称\t" + task.getName());
                System.out.println("任务执行人\t" + task.getAssignee());
                System.out.println("流程定义id\t" + task.getProcessDefinitionId());
                System.out.println("流程定义key\t"+ task.getTaskDefinitionKey());
                System.out.println("流程实例id\t" +task.getProcessInstanceId());
            }
        }
        /*
        *    任务ID	c8f8aa07-4b71-11ec-a8f0-acd564a26d7a
        *    任务名称	审批【部门经理】
        *    任务执行人	张三
        */
    }

    /*第四步：执行任务*/
    @Test
    public void complte() {
        String taskId = "5002";
        taskService.complete(taskId);
        System.out.println("任务执行完成");
    }

    /*第五步：查看历史流程实例*/
    @Test
    public void findhistProcessInstance() {
        List<HistoricProcessInstance> list = historyService.createHistoricProcessInstanceQuery()
                .processDefinitionKey(bpmnNameAndKey)
                .list();
        if (!CollectionUtils.isEmpty(list)) {
            for (HistoricProcessInstance historicProcessInstance : list) {
                System.out.println("业务系统key\t" + historicProcessInstance.getBusinessKey());
                System.out.println("部署对象ID\t" + historicProcessInstance.getDeploymentId());
                System.out.println("执行时长\t" + historicProcessInstance.getDurationInMillis());
                System.out.println("流程定义ID\t" + historicProcessInstance.getProcessDefinitionId());
                System.out.println("流程定义的key\t" + historicProcessInstance.getProcessDefinitionKey());
                System.out.println("流程定义名称\t" + historicProcessInstance.getProcessDefinitionName());
            }
        }
    }

    /*第六步：查看历史任务*/
    @Test
    public void findHisTask(){
        List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery()
                .list();
        if (!CollectionUtils.isEmpty(list)) {
            for (HistoricTaskInstance historicTaskInstance : list) {
                System.out.println("任务执行人\t" + historicTaskInstance.getAssignee());
                System.out.println("任务名称\t" + historicTaskInstance.getName());
                System.out.println("任务ID\t" + historicTaskInstance.getId());
                System.out.println("流程实例ID\t" + historicTaskInstance.getProcessInstanceId());
                System.out.println("*****************");
            }
        }
    }



}
