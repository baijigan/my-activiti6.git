package com.sun.myactiviti.j_persontask;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.springframework.stereotype.Component;

/**
 * <p></p>
 *
 * @author TT
 * @date 2021-07-29 23:45
 */
@Component
public class TaskListenerImpl implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {
        /*指定个人任务的办理人，组任务办理人*/
        /*可以查询数据库*/
        delegateTask.setAssignee("张三");

    }
}
